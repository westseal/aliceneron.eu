const {html} = require('common-tags');

function Projet({title, date, image, link, linkText, paragraphe, place, raised}) {
  return html`

  <div class="${`projet__container ${raised ? `projet__container--raised` :``}`}">
  <h3 class="projet__date">${date}</h3>
  <h2><a class="projet__link" href="${link}">${linkText}</a><h2>  

    <h3 class="projet__place">${place}</h3>
    <div class="description">
    <img class="thumbnail" src="${image}"/>
    <p class="projet__paragraphe">${paragraphe}</p>

   </div>
    
  </div>

  `;
}

module.exports = Projet;