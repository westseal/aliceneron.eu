const {html} = require('common-tags');

function Friend({title, date, image, link, linkText, paragraphe, place, raised}) {
  return html`

  <div class="${`friend__container ${raised ? `friend__container--raised` :``}`}">
  <h2><a class="friend__link" href="${link}">${linkText}</a><h2>  
    <h2 class="friend__title">${title}</h2>


   </div>
    
  </div>

  `;
}

module.exports = Friend;