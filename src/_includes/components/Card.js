const {html} = require('common-tags');

function Card({title, date, paragraphe, place, raised}) {
  return html`

  <div class="${`card__container ${raised ? `card__container--raised` :``}`}">
  <h3 class="card__date">${date}</h3>  
  <h3 class="card__place">${place}</h3>
  <h2 class="card__title">${title}</h2>
    <p class="card__paragraphe">${paragraphe}</p>

    
  </div>

  `;
}

module.exports = Card;