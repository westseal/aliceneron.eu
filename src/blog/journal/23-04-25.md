---
layout: layouts/journal.html
title: Préserver les bibliothèques
date: "2023-04-25"
---

### Éducation permanente, ateliers numériques

Les mardi après-midi, je me rend à la bibliothèque de Saint-Josse pour animer des ateliers d'éducation au numérique avec l'ARC (Action Recherche Culturelle) et le Cfep (Centre Féminin d'Éducation Permanente). Le cours se destine à un public exclusivement féminin et majoritairement immigré, qui apprend le français à côté.

Au moment de la pause, l'une des dames avec qui je discutais à propos des photos qu'elle a prise (le cours porte sur l'utilisation de l'appareil photo du smartphone), en grande précarité économique, restait émerveillée par l'écran d'information qui proposait toutes les activités (gratuites) de la bibliothèque publique; ateliers numériques, cours de langue (néerlandais), ateliers pour les enfants etc.

> Vous vous rendez-compte ? Ils font tout ça ici ! Tout ça c'est gratuit ! C'est bien ce qu'ils font pour nous ! Parce que moi j'ai pas de sous alors sinon je pourrais rien faire. Et c'est dégeulasse parce que ceux qui peuvent payer ils peuvent apprendre des choses et nous on peut pas.