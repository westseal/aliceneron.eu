---
layout: layouts/journal.html
title: Raconter la bibliothèque à quatre mains
date: "2023-05-06"
---

### Réunion/bilan autour de l'organisation du rideau de perles

L'ordre du jour et les cookies sont préparés par Lou. Martin rapporte l'imprimante thermique dans son sac isotherme. J'accueille la réunion et prépare du thé. Le raspberry pi est branché et la bibliothèque numérique est disponible en se connectant au wifi de l'appartement. Dessus, j'y ai mis des pdf de textes complets mais aussi d'extraits sélectionnés qui me paraissent intéressant pour nous aider dans nos discussions. 

Au programme :

- Rappel de tout ce qu’on a fait cette année
- Quels ont été vos ressentis au sein du collectif, vos frustrations ?
- Est ce qu'il y a des problèmes dont vous voulez parler en particulier ?
- Quelles sont vos pistes/idées pour mieux s'organiser, qu'est ce qui vous semble nécessaire de mettre en place dans le rideau ?
- Y'a t'il des aménagements dont vous pourriez avoir besoin pour personnellement vous sentir mieux au sein du rideau ? (lieu des reunions, frequences ,ect ) 
- Qu'est ce que vous avez aimé faire cette année ? 
- Quels sont vos desirs pour l'année prochaine si vous êtes encore là ?

Parce qu'aborder ces questions nécessitent que chacun·e soit apte à prendre la parole et à dire ce qu'iel souhaite dire, je réfléchis à d'autres protocoles d'expression. Je met à disposition des feuilles et des crayons.

Je propose que le rappel de ce qu'on a fait cette année passe par l'écrit : chacun·e est invité·e à écrire/dessiner les moments dont iels se souviennent. Puis on lit chacun·e nos points, sous forme de réponse aux autres. Plusieurs personnes ont noté les mêmes moments, d'autres se répondent. Finalement, ce point prend beaucoup de temps car il permet déjà d'aborder des ressentis/frustrations.

Pour le deuxième point, chacun·e est invité·e à noter/dessiner ses ressentis et frustrations. Encore une fois le dialogue est fluide, les points s'enchaînent et se répondent. Enfin, pour aborder les pistes et idées pour mieux s'organiser, dont on a déjà pu parler avec les deux autres moments, je propose la lecture d'extraits de deux textes.

Les textes (*La tyranie de l'absence de structure* de Jo Freeman et *Comment s'organiser ?* de Starhawk font écho à ce qui a été dit précedemment et formulent autrement les problèmes que l'on rencontre). Isidore aimerait bien pouvoir l'imprimer pour le lire. C'est là que l'imprimante thermique rentre en jeu. Pendant que je lis le deuxième texte, Martin imprime un exemplaire à chacun·e d'entre nous. 

<!-- ![](/assets/images/journal_de_bord/Capture d’écran du 2023-05-25 17-30-23.png)


![](/assets/images/journal_de_bord/P1010055mini.JPG)

![](/assets/images/journal_de_bord/P1010058mini.JPG)

 -->

