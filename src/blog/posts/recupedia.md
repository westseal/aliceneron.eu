---
layout: layouts/post.html
title: "Récupedia"
date: 2023-04
place: erg, Brussels
tags:
  - récupérathèque
  - collectif
  - boîte à outils

---


<section class="pictures">

![/assets/images/recupedia/interface_clair.png](/assets/images/recupedia/interface_clair.jpg)

![/assets/images/recupedia/interface_sombre.png](/assets/images/recupedia/interface_sombre.jpg)







</section>

<section class="description">

[Recupedia](https://recupedia.recuperatheque.org/?action=home&template=default), la boîte à outils des récupérathèques. Réalisé avec [Luuse](https://www.luuse.io/).


</section>