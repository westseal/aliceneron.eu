---
layout: layouts/post.html
title: De quels services transitant par les Big-Tech Cloud n'arrivons-nous pas à nous passer ? 
place: Constant, Brussels
link: /blog/journal/23-03-08
date: 2023-06
tags:
  - installation
  - grêve
  - BigTech Cloud
  - protocole

---


<section class="pictures">

![/assets/images/vitrine/P1010009.JPG](/assets/images/vitrine/P1010009.JPG)

![/assets/images/vitrine/P1010023.JPG](/assets/images/vitrine/P1010023.JPG)

![/assets/images/vitrine/20230309105955119_0004.jpg](/assets/images/vitrine/20230309105955119_0004.jpg)





</section>

<section class="description">

Protocole de conversation visant à inscrire sur des cartes les *services* informatiques qui transitent par les BigTech Cloud qui nous semblent difficiles de mettre de côté. Discussion collective depuis nos usages situés pour en dégager des formes de déviations dans l'optique d'ouvrir d'autres imaginaires socio-techniques. Avec la participation de Laurent Mbaah, Andreas Camps, Alexia de Visscher, Lionel Maes, Martin Lemaire, Mathias Redl, Elo Goldberg Jacquemain.

</section>



