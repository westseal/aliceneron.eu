---
layout: layouts/post.html
title: Constant Calibre
place: Constant
date: 2023-06
tags:
  - graphic design
  - web to print
  - digital library

---



<section class="pictures">



![Interface de la "bibliothèque"](/assets/images/constant_calibre/interface.jpg) 

![Test de graphisme pour le catalogue](/assets/images/constant_calibre/page_diversions.jpg)

![Test de graphisme pour le catalogue](/assets/images/constant_calibre/page_debian.jpg)

![Test de graphisme pour le catalogue](/assets/images/constant_calibre/page_frankenstein.jpg)

![Test de graphisme pour le catalogue](/assets/images/constant_calibre/page_repeat.jpg)

![Test de graphisme pour le catalogue](/assets/images/constant_calibre/bookmark.png)





</section>

<section class="description">

Design de l'interface, du système de cataloguage et défrichage de la base de données de la bibliothèque numérique de l'association <a href="https://constantvzw.org/site/?lang=en">Constant</a>.

## Documentation
Voir : documentation/constant_calibre

## Rencontres

**Apprendre à communiquer avec un·e serveur·euse**

- Rencontrer la·e serveur·euse qui ronfle au sous-sol
- Lui demander la permission pour échanger avec iel
- Apprendre son langage pour qu'iel me comprenne
- Faire très attention aux bonnes formulations (iel est très sensible au choix des mots)
- Flâner dans ses pensées
- Iel en sait des choses
- Lui demander si iel veut bien me transmettre ses connaissances
- La·le saluer puis repartir discrètement avec toute sa bibliothèque

**Disséquer la base de données ou comprendre comment cohabitent les livres**

- Frapper à leur porte
- S'essuyer les pieds sur le paillasson avant d'entrer
- Compter les clôtures
- Creuser des tunnels
- Ou utiliser une pince-monseigneur
- Leur construire de nouvelles maisons
- Tracer d'autres chemins
- Dessiner des ponts
- Les inciter au voyage
- Entretenir les bonnes relations de voisinnage
- Cartographier le territoire, ne pas oublier les zones inondables

**Comprendre le programme Calibre-web ou comment rendre publique la bibliothèque**

- Déchiffrer ce qu'iel raconte
- Lire, lire et relire
- Accepter de ne pas maîtriser la langue
- Et de ne pas tout comprendre
- Tenter de communiquer
- Si iel est bavard·e, tenter de la·le faire taire
- Attention parfois certaines choses qui paraissent superflux sont utiles
- Essayer de se faire pardonner
- Faire marche arrière
- Laisser faire le temps
- Lui faire des commentaires
- Réamorcer le dialogue
- Essayer d'autres tournures de phrases
- Quand la discussion est au point mort, demander de l'aide
- Garder trace des échanges

Après avoir pris le temps de rencontrer les différent·es acteur·ices du projet, je me suis finalement concentrée sur l'ébauche d'un système graphique qui fasse lien entre la bibliothèque numérique et celle, physique, de l'association. Un travail d'interfaces donc.

- Comment organiser des ressources sans les enfermer dans des catégories fixes ?
- Comment aborder la fluidité depuis une base de données ?
- Comment rendre compte de la subjectivité du processus de catégorisation d'une chose ?
- Que produit un système graphique qui sépare ce qui peut-être ordonné de ce qui peut être différencié ?
- Comment faire ressortir la diversité linguistique des publications ?
- Comment penser un système ni trop complexe, ni trop réducteur ?
- Comment donner de l'importance à la question des licences ?
- Comment donner à voir la pluralité et la multiplicité des formats de fichiers numériques ?
- Comment sortir de l'image d'une bibliothèque "virtuelle" pour considérer les objets numériques comme des entités singulières, et non de simples copies numériques ?

