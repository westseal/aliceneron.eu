---
layout: layouts/post.html
title: Je veux une présidente
place: Lyon
description: This is an example blog post
date: 2021-11
tags:
  - design graphique
  - édition
  - exposition
  - institution
  - archive
---

<section class="pictures">

![/assets/images/journal/P1010438.JPG](/assets/images/journal/P1010438.JPG)

![/assets/images/journal/IMG_20211001_151953.jpg](/assets/images/journal/IMG_20211001_151953.jpg)

![/assets/images/journal/IMG_8486.jpg](/assets/images/journal/IMG_8486.jpg)

![/assets/images/journal/IMG_20211001_151914.jpg](/assets/images/journal/IMG_20211001_151914.jpg)

![/assets/images/journal/IMG_20211001_152024.jpg](/assets/images/journal/IMG_20211001_152024.jpg)

![/assets/images/journal/journal.jpg](/assets/images/journal/journal.jpg)

![/assets/images/journal/P1010342.JPG](/assets/images/journal/P1010342.JPG)

![/assets/images/journal/cartespostales.jpg](/assets/images/journal/cartespostales.jpg)




</section>


<section class="description">

Journal recensant les commentaires sexistes et homophobes des politicien·nes en
France depuis 1998 et les confrontant au poème I want a president de Zoe Leonard écrit la même année. Archive de l'état des lieux social, juridique et politique du pouvoir dominant masculin dans la société française durant les deux dernières décennies. Exposé dans la mairie de la Croix Rousse (Lyon) à l'occasion du 25 novembre, journée de lutte contre les violences faites aux femmes, il est accompagné d'ouvrages apportés par une bibliothèque féministe locale et activé par les élu·es qui slamant le poème. Il donne lieu à la [citadelle des mâles](https://mairie4.lyon.fr/actualite/culture/la-citadelle-des-males-conference-debat); une conférence autour du sexisme structurel en politique.


