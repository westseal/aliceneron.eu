---
layout: layouts/post.html
title: "La grange : Laisser travailler les récits 1h10 à 15 degrès"
date: 2023-04
link: /blog/journal/23-04-26
place: erg, Brussels
tags:
  - participatif
  - workshop
  - communauthèque
  - web to print
  - licenses
  - open source
---


<section class="pictures">

![/assets/images/workshop/20230428174809371_0002.jpg](/assets/images/workshop/espace.jpg)

![/assets/images/workshop/20230428175421289_0001.jpg](/assets/images/workshop/document.jpg)

![/assets/images/workshop/20230428175421289_0001.jpg](/assets/images/workshop/depot.jpg)

![/assets/images/workshop/20230428175421289_0001.jpg](/assets/images/workshop/notes.jpg)

</section>

<section class="description">

Workshop organisé avec les étudiant·es d'installation performance consistant à discuter du (non)partage de nos savoirs artistiques. Pourquoi et comment partager les outils, savoirs-faire, mediums, références, objet/sujet, matières, processus en oeuvre dans ce qu'on/qui nous travaille ?

Prenant comme point de départ l'open source, trois espaces sont dessinés à l'intérieur desquels sont disséminées les ressources imprimées depuis la bibliothèque numérique; Propriété intellectuelle et droits d'auteur·ice, Licences et conditions de diffusion et de réutilisation puis Partager ou ne pas partager; accès ouvert et appropriation. Le dispositif de conversation invite les participant·es à apporter une chose faisant parti de leurs pratiques avec la quelle iels rencontrent des questions de (non)partage. 

Chacun·e est amené·e à remplir une fiche de dépot de cette chose dans la bibliothèque numérique en y inscrivant ses métadonnées. Démarrage des conversations à partir des métadonnées. Les objets éditoriaux facilitent les dynamiques d’échange et de partage entre les participant·es qui sont invité·es à s’approprier les ressources mises à disposition pour raconter leurs expériences subjectives et tisser du lien autour du sujet. Le dispositif s’appuie sur l’approche féministe de l’autonomie relationnelle, en faisant de cet espace un lieu où les personnes lisent et discutent ensemble.



</section>

